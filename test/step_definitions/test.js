const {Given, Then, When, And} = require('cucumber');
const { mockServer, stopStart } = require("./mockttp");
const fs = require("fs");
const path = require("path");

const readFile = (path, opts = 'utf8') =>
  new Promise((resolve, reject) => {
    fs.readFile(path, opts, (err, data) => {
      if (err) reject(err)
      else resolve(data)
    })
  });

  // Given(/^I setup the app$/, async () => {
  //   mockServer.start(4040);
  //   console.log("Mockttp has started");
  //   const data1 = await readFile(path.join(__dirname, '../../mockttp/mappings/cms/content/experience-fragments/pli/en/native_app/tabcontactus/_default/screen.model.json'));
  //   await mockServer.get("/content/experience-fragments/pli/en/native_app/tabcontactus/screen.model.json").thenReply(200, data1);
  //   const data2 = await readFile(path.join(__dirname, '../../mockttp/mappings/dbg/rules/games.json'));
  //   await mockServer.get("/dbg/games").thenReply(200, data2);
  // });


  Given(/^I set up the api with results$/, () => {
    //check for a selector
    browser.pause(20000);
  });
  
  Given(/^I visit home page$/, () => {
    browser.pause(2000);
    let el = $("~contactform");
    el.isVisible().should.equal(true);
  });
  
  Given(/^I set up the api with real results$/, async () => {
    const data = await readFile(path.join(__dirname, '../../mockttp/mappings/cms/content/experience-fragments/pli/en/native_app/tabcontactus/_default/screen.model.json'));
    await mockServer.get("/content/experience-fragments/pli/en/native_app/tabcontactus/screen.model.json").thenReply(200, data);
    browser.pause(5000);
  });
  
  Given(/^I set up the api with empty results$/, async () => {
    const data = await readFile(path.join(__dirname, '../../mockttp/mappings/cms/content/experience-fragments/pli/en/native_app/tabcontactus/_empty/screen.model.json'));
    await mockServer.get("/content/experience-fragments/pli/en/native_app/tabcontactus/screen.model.json").thenReply(200, data);
    browser.pause(5000);
  });
  
  Given(/^I wait "([^"]*)" seconds$/, (secs) => {
    browser.pause(secs * 1000);
  });
  
  Given(/^I refresh the app$/, () => {
    browser.refresh();
  });
  
  Given(/^I should see results$/, () => {
    console.log('running I should see results scenario');
    let el = $("~test-contactform");
    el.isVisible().should.equal(true);
  });
  
  Given(/^I should see empty results$/, () => {
    browser.pause(2000);
    let el = $("~test-contactform");
    el.isVisible().should.equal(false);
  });
  
  Given(/^I should click on "(.*)" tab$/, (tabName) => {
    browser.pause(2000);
    let el = $("~test-" + tabName);
    el.click();
  });
  
  // Given(/^I reload the app$/, () => {
  //   browser.reload();
  // });


