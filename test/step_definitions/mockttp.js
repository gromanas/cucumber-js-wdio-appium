const fs = require("fs");
const path = require("path");
const kill = require('kill-port');
const http = require('http');
const port = 4040;
const mockServer = require("mockttp").getLocal();

const readFile = (path, opts = 'utf8') =>
  new Promise((resolve, reject) => {
    fs.readFile(path, opts, (err, data) => {
      if (err) reject(err)
      else resolve(data)
    })
  });

function stopStart(port) {
  return kill(port)
  .then(console.log('successfully killed port'))
  .then(() => mockServer.start(port))
  .then(() => console.log('applying initial mocks'))
  .then(applyFirstLoadMocks)
  .then(() => console.log('mock server started'));
};

async function applyFirstLoadMocks() {
  let data;
  data = await readFile(path.join(__dirname, '../../mockttp/mappings/cms/content/experience-fragments/pli/en/native_app/tabcontactus/_default/screen.model.json'));
  await mockServer.get("/content/experience-fragments/pli/en/native_app/tabcontactus/screen.model.json").thenReply(200, data);
  
  data = await readFile(path.join(__dirname, '../../mockttp/mappings/dbg/rules/games.json'));
  await mockServer.get("/dbg/games").thenReply(200, data);
}

stopStart(4040);

exports.stopStart = stopStart;
exports.mockServer = mockServer;