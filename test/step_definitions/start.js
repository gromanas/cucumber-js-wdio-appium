const mockServer = require("mockttp").getLocal();
const fs = require("fs");
const path = require("path");

const readFile = (path, opts = 'utf8') =>
  new Promise((resolve, reject) => {
    fs.readFile(path, opts, (err, data) => {
      if (err) reject(err)
      else resolve(data)
    })
  });

async function applyFirstLoadMocks() {
  let data;
  data = await readFile(path.join(__dirname, '../../mockttp/mappings/cms/content/experience-fragments/pli/en/native_app/tabcontactus/_default/screen.model.json'));
  await mockServer.get("/content/experience-fragments/pli/en/native_app/tabcontactus/screen.model.json").thenReply(200, data);
  
  data = await readFile(path.join(__dirname, '../../mockttp/mappings/dbg/rules/games.json'));
  await mockServer.get("/dbg/games").thenReply(200, data);
}

mockServer.start(4040).then(applyFirstLoadMocks);
 