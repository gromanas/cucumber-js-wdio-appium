/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import 'react-native';
import React from 'react';
import App from '../App';
import ShallowRenderer from 'react-test-renderer/shallow';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const renderer = new ShallowRenderer();
  expect(renderer.render(<App />)).toMatchSnapshot();
});
